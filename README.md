# Usage

1. Import tokens in multiple file format into `tokens-studio-tokens`
2. Run `node transformTokensStudio.js` to generate style-dictionary accepted `transformed-tokens` directory files
3. Run `node build.js` to generate files for each platform
4. Increment the version on `package.json`
5. Push changes to Gitlab
6. Publish changes via `npm publish`
7. Use package in another repository by running `npm install maven-hackathon-design-tokens`