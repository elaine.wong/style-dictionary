const StyleDictionaryPackage = require("style-dictionary");
const fs = require("fs");

// HAVE THE STYLE DICTIONARY CONFIG DYNAMICALLY GENERATED

const OUTPUT_DIRECTORY = "build";

function getStyleDictionaryConfig(theme) {
  const themeCap = theme.charAt(0).toUpperCase() + theme.slice(1)
  return {
    source: [`transformed-tokens/*/${theme}.json`],
    platforms: {
      css: {
        transformGroup: "css",
        buildPath: `${OUTPUT_DIRECTORY}/css/${theme}/`,
        files: [
          {
            destination: "mvn-tokens.module.css",
            format: "css/variables",
          },
        ],
        prefix: "mvn",
      },
      js: {
        transformGroup: "js",
        buildPath: `${OUTPUT_DIRECTORY}/ts/${theme}/`,
        files: [
          {
            destination: "colors.js",
            format: "javascript/es6",
            filter: {
              attributes: {
                category: "color",
              },
            },
          },
          {
            destination: "spacing.js",
            format: "javascript/es6",
            filter: {
              attributes: {
                category: "size",
              },
            },
          },
        ],
        prefix: "mvn",
      },
      ts: {
        transformGroup: "js",
        buildPath: `${OUTPUT_DIRECTORY}/ts/${theme}/`,
        files: [
          {
            destination: "colors.d.ts",
            format: "typescript/es6-declarations",
            filter: {
              attributes: {
                category: "color",
              },
            },
          },
          {
            destination: "spacing.d.ts",
            format: "typescript/es6-declarations",
            filter: {
              attributes: {
                category: "size",
              },
            },
          },
        ],
        prefix: "mvn",
      },
      android: {
        transformGroup: "compose",
        buildPath: `${OUTPUT_DIRECTORY}/android/${theme}/`,
        files: [
          {
            destination: `Mvn${themeCap}Color.kt`,
            format: "compose/object",
            className: `Mvn${themeCap}Color`,
            packageName: `Mvn${themeCap}Color`,
            filter: {
              attributes: {
                category: "color",
              },
            },
          },
          {
            destination: "MvnSpacing.kt",
            format: "compose/object",
            className: "MvnSpacing",
            packageName: "MvnSpacing",
            type: "int",
            filter: {
              attributes: {
                category: "size",
              },
            },
          },
        ],
        prefix: "mvn",
      },
      ios: {
        transformGroup: "ios-swift-separate",
        buildPath: `${OUTPUT_DIRECTORY}/ios/${theme}/`,
        files: [
          {
            destination: `Mvn${themeCap}Color.swift`,
            format: "ios-swift/enum.swift",
            className: `Mvn${themeCap}Color`,
            filter: {
              attributes: {
                category: "color",
              },
            },
          },
          {
            destination: "MvnSpacing.swift",
            format: "ios-swift/enum.swift",
            className: "MvnSpacing",
            filter: {
              attributes: {
                category: "size",
              },
            },
          },
        ],
        prefix: "mvn",
      },
    },
  };
}

console.log("Build started...");

if (fs.existsSync(`./${OUTPUT_DIRECTORY}`)) {
  fs.rmSync(`./${OUTPUT_DIRECTORY}`, { recursive: true, force: true });
}
fs.mkdirSync(`./${OUTPUT_DIRECTORY}`);

// PROCESS THE DESIGN TOKENS FOR THE DIFFERENT THEMES

const data = require("./tokens-studio-tokens/$metadata.json");

const themes = data.tokenSetOrder;

themes.map(function (theme) {
  ["css", "js", "ts", "ios", "android"].map(function (platform) {
    console.log("\n==============================================");
    console.log(`\nProcessing: [${platform}] [${theme}]`);

    const StyleDictionary = StyleDictionaryPackage.extend(
      getStyleDictionaryConfig(theme)
    );

    StyleDictionary.buildPlatform(platform);

    console.log("\nEnd processing");
  });
});

console.log("\n==============================================");
console.log("\nBuild completed!");
