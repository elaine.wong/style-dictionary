
//
// MvnDarkColor.swift
//

// Do not edit directly
// Generated on Thu, 09 May 2024 15:05:10 GMT


import UIKit

public enum MvnDarkColor {
    public static let mvnBackground = UIColor(red: 0.098, green: 0.094, blue: 0.090, alpha: 1)
    public static let mvnBackgroundSecondary = UIColor(red: 0.000, green: 0.000, blue: 0.000, alpha: 1)
    public static let mvnBlueContrast = UIColor(red: 0.796, green: 0.902, blue: 1.000, alpha: 1)
    public static let mvnBlueSecondary = UIColor(red: 0.380, green: 0.627, blue: 1.000, alpha: 1)
    public static let mvnCoralContrast = UIColor(red: 1.000, green: 0.800, blue: 0.718, alpha: 1)
    public static let mvnCoralSecondary = UIColor(red: 1.000, green: 0.537, blue: 0.400, alpha: 1)
    public static let mvnElementStroke = UIColor(red: 0.267, green: 0.259, blue: 0.243, alpha: 1)
    public static let mvnGreenContrast = UIColor(red: 0.961, green: 1.000, blue: 0.976, alpha: 1)
    public static let mvnGreenSecondary = UIColor(red: 0.443, green: 0.820, blue: 0.710, alpha: 1)
    public static let mvnInteractiveBrand = UIColor(red: 1.000, green: 1.000, blue: 1.000, alpha: 1)
    public static let mvnPurpleContrast = UIColor(red: 0.871, green: 0.839, blue: 1.000, alpha: 1)
    public static let mvnPurpleSecondary = UIColor(red: 0.671, green: 0.643, blue: 1.000, alpha: 1)
    public static let mvnTextDefault = UIColor(red: 0.973, green: 0.969, blue: 0.969, alpha: 1)
    public static let mvnTextSecondary = UIColor(red: 0.812, green: 0.804, blue: 0.792, alpha: 1)
    public static let mvnTextTertiary = UIColor(red: 0.729, green: 0.718, blue: 0.702, alpha: 1)
    public static let mvnWhite = UIColor(red: 1.000, green: 1.000, blue: 1.000, alpha: 1)
}
