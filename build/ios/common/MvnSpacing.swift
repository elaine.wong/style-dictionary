
//
// MvnSpacing.swift
//

// Do not edit directly
// Generated on Thu, 09 May 2024 15:05:10 GMT


import UIKit

public enum MvnSpacing {
    public static let mvnSpacingDefault = CGFloat(256.00)
    public static let mvnSpacingLarge = CGFloat(384.00)
    public static let mvnSpacingNone = CGFloat(0.00)
    public static let mvnSpacingSmall = CGFloat(192.00)
    public static let mvnSpacingXLarge = CGFloat(512.00)
    public static let mvnSpacingXSmall = CGFloat(128.00)
    public static let mvnSpacingXxLarge = CGFloat(640.00)
    public static let mvnSpacingXxSmall = CGFloat(64.00)
    public static let mvnSpacingXxxLarge = CGFloat(768.00)
}
