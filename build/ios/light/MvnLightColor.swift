
//
// MvnLightColor.swift
//

// Do not edit directly
// Generated on Thu, 09 May 2024 15:05:10 GMT


import UIKit

public enum MvnLightColor {
    public static let mvnBackground = UIColor(red: 0.965, green: 0.961, blue: 0.949, alpha: 1)
    public static let mvnBackgroundSecondary = UIColor(red: 1.000, green: 1.000, blue: 1.000, alpha: 1)
    public static let mvnBlack = UIColor(red: 0.000, green: 0.000, blue: 0.000, alpha: 1)
    public static let mvnBlueContrast = UIColor(red: 0.008, green: 0.278, blue: 0.478, alpha: 1)
    public static let mvnBlueSecondary = UIColor(red: 0.380, green: 0.627, blue: 1.000, alpha: 1)
    public static let mvnCoralContrast = UIColor(red: 1.000, green: 0.337, blue: 0.192, alpha: 1)
    public static let mvnCoralSecondary = UIColor(red: 1.000, green: 0.537, blue: 0.400, alpha: 1)
    public static let mvnElementStroke = UIColor(red: 0.890, green: 0.886, blue: 0.878, alpha: 1)
    public static let mvnGreenContrast = UIColor(red: 0.012, green: 0.341, blue: 0.282, alpha: 1)
    public static let mvnGreenSecondary = UIColor(red: 0.443, green: 0.820, blue: 0.710, alpha: 1)
    public static let mvnInteractiveBrand = UIColor(red: 0.008, green: 0.549, blue: 0.455, alpha: 1)
    public static let mvnPurpleContrast = UIColor(red: 0.149, green: 0.141, blue: 0.545, alpha: 1)
    public static let mvnPurpleSecondary = UIColor(red: 0.671, green: 0.643, blue: 1.000, alpha: 1)
    public static let mvnTextDefault = UIColor(red: 0.098, green: 0.094, blue: 0.090, alpha: 1)
    public static let mvnTextSecondary = UIColor(red: 0.180, green: 0.173, blue: 0.165, alpha: 1)
    public static let mvnTextTertiary = UIColor(red: 0.361, green: 0.349, blue: 0.329, alpha: 1)
}
