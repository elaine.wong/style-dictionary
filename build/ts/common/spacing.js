/**
 * Do not edit directly
 * Generated on Thu, 09 May 2024 15:05:10 GMT
 */

export const MvnSizeSpacingNone = "0rem";
export const MvnSizeSpacingXxSmall = "4rem";
export const MvnSizeSpacingXSmall = "8rem";
export const MvnSizeSpacingSmall = "12rem";
export const MvnSizeSpacingDefault = "16rem";
export const MvnSizeSpacingLarge = "24rem";
export const MvnSizeSpacingXLarge = "32rem";
export const MvnSizeSpacingXxLarge = "40rem";
export const MvnSizeSpacingXxxLarge = "48rem";
