/**
 * Do not edit directly
 * Generated on Thu, 09 May 2024 15:05:10 GMT
 */

export const MvnSizeSpacingNone : string;
export const MvnSizeSpacingXxSmall : string;
export const MvnSizeSpacingXSmall : string;
export const MvnSizeSpacingSmall : string;
export const MvnSizeSpacingDefault : string;
export const MvnSizeSpacingLarge : string;
export const MvnSizeSpacingXLarge : string;
export const MvnSizeSpacingXxLarge : string;
export const MvnSizeSpacingXxxLarge : string;
