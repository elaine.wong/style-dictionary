/**
 * Do not edit directly
 * Generated on Thu, 09 May 2024 15:05:10 GMT
 */

export const MvnColorTextDefault = "#f8f7f7";
export const MvnColorTextSecondary = "#cfcdca";
export const MvnColorTextTertiary = "#bab7b3";
export const MvnColorElementStroke = "#44423e";
export const MvnColorBackground = "#191817";
export const MvnColorInteractiveBrand = "#ffffff";
export const MvnColorGreenContrast = "#f5fff9";
export const MvnColorGreenSecondary = "#71d1b5";
export const MvnColorBlueContrast = "#cbe6ff";
export const MvnColorBlueSecondary = "#61a0ff";
export const MvnColorCoralContrast = "#ffccb7";
export const MvnColorCoralSecondary = "#ff8966";
export const MvnColorPurpleContrast = "#ded6ff";
export const MvnColorPurpleSecondary = "#aba4ff";
export const MvnColorBackgroundSecondary = "#000000";
export const MvnColorWhite = "#ffffff";
