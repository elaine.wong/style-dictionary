/**
 * Do not edit directly
 * Generated on Thu, 09 May 2024 15:05:10 GMT
 */

export const MvnColorTextDefault = "#191817";
export const MvnColorTextSecondary = "#2e2c2a";
export const MvnColorTextTertiary = "#5c5954";
export const MvnColorElementStroke = "#e3e2e0";
export const MvnColorBackground = "#f6f5f2";
export const MvnColorInteractiveBrand = "#028c74";
export const MvnColorGreenContrast = "#035748";
export const MvnColorGreenSecondary = "#71d1b5";
export const MvnColorBlueContrast = "#02477a";
export const MvnColorBlueSecondary = "#61a0ff";
export const MvnColorCoralContrast = "#ff5631";
export const MvnColorCoralSecondary = "#ff8966";
export const MvnColorPurpleContrast = "#26248b";
export const MvnColorPurpleSecondary = "#aba4ff";
export const MvnColorBlack = "#000000";
export const MvnColorBackgroundSecondary = "#ffffff";
