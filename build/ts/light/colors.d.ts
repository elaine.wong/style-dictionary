/**
 * Do not edit directly
 * Generated on Thu, 09 May 2024 15:05:10 GMT
 */

export const MvnColorTextDefault : string;
export const MvnColorTextSecondary : string;
export const MvnColorTextTertiary : string;
export const MvnColorElementStroke : string;
export const MvnColorBackground : string;
export const MvnColorInteractiveBrand : string;
export const MvnColorGreenContrast : string;
export const MvnColorGreenSecondary : string;
export const MvnColorBlueContrast : string;
export const MvnColorBlueSecondary : string;
export const MvnColorCoralContrast : string;
export const MvnColorCoralSecondary : string;
export const MvnColorPurpleContrast : string;
export const MvnColorPurpleSecondary : string;
export const MvnColorBlack : string;
export const MvnColorBackgroundSecondary : string;
