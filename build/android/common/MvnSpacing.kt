

// Do not edit directly
// Generated on Thu, 09 May 2024 15:05:10 GMT



package MvnSpacing;

import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.*

object MvnSpacing {
  val mvnSizeSpacingDefault = 256.00.dp
  val mvnSizeSpacingLarge = 384.00.dp
  val mvnSizeSpacingNone = 0.00.dp
  val mvnSizeSpacingSmall = 192.00.dp
  val mvnSizeSpacingXLarge = 512.00.dp
  val mvnSizeSpacingXSmall = 128.00.dp
  val mvnSizeSpacingXxLarge = 640.00.dp
  val mvnSizeSpacingXxSmall = 64.00.dp
  val mvnSizeSpacingXxxLarge = 768.00.dp
}
