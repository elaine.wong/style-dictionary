

// Do not edit directly
// Generated on Thu, 09 May 2024 15:05:10 GMT



package MvnLightColor;

import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.*

object MvnLightColor {
  val mvnColorBackground = Color(0xfff6f5f2)
  val mvnColorBackgroundSecondary = Color(0xffffffff)
  val mvnColorBlack = Color(0xff000000)
  val mvnColorBlueContrast = Color(0xff02477a)
  val mvnColorBlueSecondary = Color(0xff61a0ff)
  val mvnColorCoralContrast = Color(0xffff5631)
  val mvnColorCoralSecondary = Color(0xffff8966)
  val mvnColorElementStroke = Color(0xffe3e2e0)
  val mvnColorGreenContrast = Color(0xff035748)
  val mvnColorGreenSecondary = Color(0xff71d1b5)
  val mvnColorInteractiveBrand = Color(0xff028c74)
  val mvnColorPurpleContrast = Color(0xff26248b)
  val mvnColorPurpleSecondary = Color(0xffaba4ff)
  val mvnColorTextDefault = Color(0xff191817)
  val mvnColorTextSecondary = Color(0xff2e2c2a)
  val mvnColorTextTertiary = Color(0xff5c5954)
}
