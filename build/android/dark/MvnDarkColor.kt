

// Do not edit directly
// Generated on Thu, 09 May 2024 15:05:10 GMT



package MvnDarkColor;

import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.*

object MvnDarkColor {
  val mvnColorBackground = Color(0xff191817)
  val mvnColorBackgroundSecondary = Color(0xff000000)
  val mvnColorBlueContrast = Color(0xffcbe6ff)
  val mvnColorBlueSecondary = Color(0xff61a0ff)
  val mvnColorCoralContrast = Color(0xffffccb7)
  val mvnColorCoralSecondary = Color(0xffff8966)
  val mvnColorElementStroke = Color(0xff44423e)
  val mvnColorGreenContrast = Color(0xfff5fff9)
  val mvnColorGreenSecondary = Color(0xff71d1b5)
  val mvnColorInteractiveBrand = Color(0xffffffff)
  val mvnColorPurpleContrast = Color(0xffded6ff)
  val mvnColorPurpleSecondary = Color(0xffaba4ff)
  val mvnColorTextDefault = Color(0xfff8f7f7)
  val mvnColorTextSecondary = Color(0xffcfcdca)
  val mvnColorTextTertiary = Color(0xffbab7b3)
  val mvnColorWhite = Color(0xffffffff)
}
