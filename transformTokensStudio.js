"use strict";

const TRANSFORM_PATH = "./transformed-tokens";

const fs = require("fs");
console.info("Transforming Tokens Studio tokens into style-dictionary format");

if (fs.existsSync(TRANSFORM_PATH)) {
  fs.rmSync(TRANSFORM_PATH, { recursive: true, force: true });
}
fs.mkdirSync(TRANSFORM_PATH);

const data = require("./tokens-studio-tokens/$metadata.json");

const themes = data.tokenSetOrder;

const types = new Map();

themes.forEach((theme) => {
  const themeData = require(`./tokens-studio-tokens/${theme}.json`);

  Object.entries(themeData).forEach(([key, value]) => {
    const type = value.type === "spacing" ? "size" : value.type
    if (!types.get(type)) {
      types.set(type, {});
    }
    if (!types.get(type)[theme]) {
      types.get(type)[theme] = {};
    }
    types.get(type)[theme][key] = value;
  });

  Array.from(types).forEach(([type, value]) => {
    Object.entries(value).forEach(([theme, themeValue]) => {
      var dir = `${TRANSFORM_PATH}/${type}`;
      if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
      }

      fs.writeFileSync(
        `${TRANSFORM_PATH}/${type}/${theme}.json`,
        JSON.stringify({ [type]:  themeValue }, null, 4)
      );
    });
  });
});

console.log("\n==============================================");
console.info("Done! See the transformed-tokens directory for output")